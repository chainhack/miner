#!/bin/bash

set -euo pipefail
trap "echo 'error: Script failed: see failed command above'" ERR

echo "sbt assembly"
sbt assembly

echo "native-image --static -H:IncludeResources='.*' -H:ReflectionConfigurationFiles=graal-native-reflect.json -jar target/scala-2.12/miner-assembly-0.1.0-SNAPSHOT.jar"
native-image \
    --static \
    -H:IncludeResources='.*' \
    -H:ReflectionConfigurationFiles=graal-native-reflect.json \
    -jar target/scala-2.12/miner-assembly-0.1.0-SNAPSHOT.jar