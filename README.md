# Miner

Chain Hack project miner

## Build
Since we run it on local machines of miners, the startup time matters.

We solve the JVM's slow startup time issue by creating a native executables using [GraalVM](https://www.graalvm.org)

The GraalVM still in the raw state and may fail with errors, in this case create an ordinary `.jar` as usual.

**Prerequisites:**
* [GraalVM](https://www.graalvm.org/downloads/) - the `native-build` script from the GraalVM's `bin` dir should be in the `PATH`
* [Docker](https://www.docker.com)
* [Chain Hack solutions tester Docker image](https://gitlab.com/chainhack/solutiontester)

`./build-native-executable.sh` (haven't tested yet)

## Current issues
Grep the code in the [src](/src) dir for `TODO` and `???`