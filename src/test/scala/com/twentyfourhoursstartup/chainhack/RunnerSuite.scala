package com.twentyfourhoursstartup.chainhack
import cats.Id
import com.twentyfourhoursstartup.chainhack.domainmodels.{MinerTask, Result}
import org.scalatest.FunSuite
import org.scalatest.Matchers._

class RunnerSuite extends FunSuite {
  val exampleTask: String =
    """
      |import random
      |from typing import Generator
      |from test_cases import TestEntity
      |
      |
      |def gen_tests(*args, **kwargs) -> Generator[TestEntity, None, None]:
      |    tests_count = random.randint(10, 20)
      |    for _ in range(tests_count):
      |        input_list = [random.randint(-10, 10) for _ in range(random.randrange(10, 100))]
      |
      |        yield TestEntity(
      |            args=[input_list],
      |            kwargs={},
      |            result=sum(input_list),
      |            time_limit=10)
      |
    """.stripMargin

  test("returns Result.Success if solution correct") {
    val task = MinerTask(
      taskId = 0,
      solutionId = 0,
      task = exampleTask,
      solution = """
          |def solution(int_array, *args, **kwargs):
          |    return sum(int_array)
          |
        """.stripMargin
    )

    Runner.run[Id](task) shouldBe Right(Result.Valid(task))
  }

  test("returns Result.Failure if solution incorrect") {
    val task = MinerTask(
      taskId = 0,
      solutionId = 0,
      task = exampleTask,
      solution = """
                   |def solution(int_array, *args, **kwargs):
                   |    return sum(int_array) + 1
                   |
        """.stripMargin
    )

    Runner.run[Id](task) shouldBe Right(Result.Invalid(task))
  }
}
