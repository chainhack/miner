package com.twentyfourhoursstartup.chainhack
import cats.Applicative
import cats.syntax.either._
import com.twentyfourhoursstartup.chainhack.domainmodels.{MinerTask, Result}

import scala.language.higherKinds

object Runner {
  def run[F[_]: Applicative](
      minerTask: MinerTask): F[Either[(MinerTask, Throwable), Result]] =
    Applicative[F].pure {
      val prefix = os.pwd / minerTask.id
      os.remove.all(prefix)
      os.makeDir(prefix)
      val genPy = prefix / "gen.py"
      val solutionPy = prefix / "solution.py"
      os.write(genPy, minerTask.task)
      os.write(solutionPy, minerTask.solution)
      val result = os
        .proc(
          "docker",
          "run",
          "--rm",
          s"--name=mining-${minerTask.taskId}-${minerTask.solutionId}",
          "-v",
          genPy.toString() + ":" + "/app/gen.py",
          "-v",
          solutionPy.toString() + ":" + "/app/solution.py",
          "registry.gitlab.com/chainhack/solutiontester"
        )
        .call(check = false)
      os.remove.all(prefix)
      result.exitCode match {
        case 0 => Result.Valid(minerTask).asRight[(MinerTask, Throwable)]
        case 1 => Result.Invalid(minerTask).asRight[(MinerTask, Throwable)]
        case x =>
          (minerTask, new RuntimeException(s"Wrong exit code: $x"))
            .asLeft[Result]
      }
    }
}
