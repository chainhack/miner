package com.twentyfourhoursstartup.chainhack
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}
import cats.effect.IO
import com.twentyfourhoursstartup.chainhack.domainmodels.{MinerTask, Result}
import org.slf4j.{Logger, LoggerFactory}
import Utils._

object Main {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    CommandLineConfig.parser
      .parse(args, CommandLineConfig.initial)
      .foreach(processing)
  }

  def processing(config: CommandLineConfig): Unit = {
    implicit val actorSystem: ActorSystem = ActorSystem()
    implicit val actorMaterializer: ActorMaterializer = ActorMaterializer()

    val minerTasks: Source[MinerTask, NotUsed] =
      Source.fromPublisher(MinerTasksFetcher.subscribe(config))
    val runnerFlow: Flow[MinerTask, Result, NotUsed] = Flow[MinerTask]
      .mapAsync(config.concurrentExecutions)(minerTask =>
        Runner.run[IO](minerTask).unsafeToFuture())
      .flatMapConcat {
        case Right(result) => Source.single(result)
        case Left((minerTask, exception)) =>
          logger.error(
            s"Some error occurred during running of miner task: $minerTask",
            exception
          )
          Source.empty
      }

    val resultsSender: Sink[Result, NotUsed] =
      Sink.fromSubscriber(ResultsSender.build(config))

    val graph = minerTasks
      .via(runnerFlow)
      .to(resultsSender)
    graph.run().discard()
  }
}
