package com.twentyfourhoursstartup.chainhack

object Utils {
  final implicit class AnyOps[A](val any: A) extends AnyVal {
    @specialized def discard(): Unit = {
      val _: A = any
      () //Return unit to prevent warning due to discarding value
    }
  }
}
