package com.twentyfourhoursstartup.chainhack
import scopt.OptionParser

final case class CommandLineConfig(concurrentExecutions: Int)

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
object CommandLineConfig {
  val initial = CommandLineConfig(0)

  val parser: OptionParser[CommandLineConfig] =
    new scopt.OptionParser[CommandLineConfig]("chainhack-miner") {
      help("help") text "prints this help"
      opt[Int]("parallelism") withFallback (() => 10) action { (x, c) =>
        c.copy(concurrentExecutions = x)
      } text "(optional) amount of concurrently running mining tasks, default is 10"
    }
}
