package com.twentyfourhoursstartup.chainhack.domainmodels

final case class MinerTask(taskId: Int,
                           solutionId: Int,
                           task: String,
                           solution: String) {
  val id: String = taskId.toString + "-" + solutionId.toString
}
