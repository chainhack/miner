package com.twentyfourhoursstartup.chainhack.domainmodels

sealed trait Result extends Product with Serializable {
  def task: MinerTask
}
object Result {
  final case class Valid(task: MinerTask) extends Result
  final case class Invalid(task: MinerTask) extends Result
}
